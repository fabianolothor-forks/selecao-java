package br.com.fabianolothor.api.indra.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.State;

public interface StateRepository extends CrudRepository<State, String>{

}
