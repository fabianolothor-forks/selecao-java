package br.com.fabianolothor.api.indra.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.GasStation;

public interface GasStationRepository extends CrudRepository<GasStation, Integer>{

}
