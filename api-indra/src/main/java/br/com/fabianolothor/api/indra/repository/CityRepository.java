package br.com.fabianolothor.api.indra.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.City;

public interface CityRepository extends CrudRepository<City, Integer>{

	public Optional<City> findByName(String name);
	
}
