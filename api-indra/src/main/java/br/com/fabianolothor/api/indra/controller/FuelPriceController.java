package br.com.fabianolothor.api.indra.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.fabianolothor.api.indra.model.FuelPrice;
import br.com.fabianolothor.api.indra.service.FuelPriceService;
import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/fuel-prices")
public class FuelPriceController {

	@Autowired
	FuelPriceService fuelPriceService;

	@ApiOperation(value = "Adds a Fuel Price")
	@PostMapping(value = "")
	public void addFuelPrice(@RequestBody FuelPrice fuelPrice) {
		fuelPriceService.addFuelPrice(fuelPrice);
	}

	@ApiOperation(value = "Removes a Fuel Price")
	@DeleteMapping(value = "/{id}")
	public void deleteFuelPrice(@PathVariable Integer id) {
		fuelPriceService.deleteFuelPrice(id);
	}

	@ApiOperation(response = FuelPrice.class, value = "Finds a Fuel Price")
	@GetMapping(value = "/{id}")
	public FuelPrice getFuelPrice(@PathVariable Integer id) {
		return fuelPriceService.getFuelPrice(id);
	}

	@ApiOperation(response = List.class, value = "Get all Fuel Prices - Available, parameters: [region, groupBy ['gasStation', 'date']]")
	@GetMapping(value = "")
	public List<FuelPrice> getAllFuelPrices(@RequestParam(value = "region", required = false) String region) {
		List<FuelPrice> response;

		if (region == null) {
			response = fuelPriceService.getAllFuelPrices();
		} else {
			response = fuelPriceService.getAllFuelPricesByRegion(region);
		}

		return response;
	}

	@ApiOperation(response = Double.class, value = "Get the average price of Fuel Prices")
	@GetMapping(value = "/average")
	public Double getAverage() {
		return fuelPriceService.getAveragePrice();
	}

	@ApiOperation(response = Double.class, value = "Get the average price of Fuel Prices by City name")
	@GetMapping(value = "/average/{cityName}")
	public Double getAverageByCityName(@PathVariable String cityName) {
		return fuelPriceService.getAveragePriceByCityName(cityName);
	}

	@ApiOperation(response = HashMap.class, value = "Get all Fuel Prices by Verification Date")
	@GetMapping(value = "/by-date")
	public HashMap<Date, List<FuelPrice>> getAllFuelPricesByDate() {
		HashMap<Date, List<FuelPrice>> response = new HashMap<Date, List<FuelPrice>>();

		List<Date> verificationDates = fuelPriceService.getAllDistinctVerificationDates();
		
		verificationDates.forEach(verificationDate -> {
			response.put(verificationDate, fuelPriceService.getAllByVerificationDate(verificationDate));
		});

		return response;
	}

	@ApiOperation(value = "Import a File of Fuel Prices from a Multipart FORM")
	@PostMapping(value = "/import", consumes = "multipart/form-data")
	public void importFuelPriceRecords(@RequestParam("file") MultipartFile file) throws IOException {
		System.out.println("Starting Import...\n\n");

		fuelPriceService.importFuelPriceRecords(file);

		System.out.println("Import Finished!\n\n");
	}

	@ApiOperation(value = "Updates a Fuel Price")
	@PutMapping(value = "")
	public void updateFuelPrice(@RequestBody FuelPrice fuelPrice) {
		fuelPriceService.updateFuelPrice(fuelPrice);
	}

	@ApiOperation(value = "Updates a Fuel Price")
	@PutMapping(value = "/{id}")
	public void updateFuelPrice(@PathVariable Integer id, @RequestBody FuelPrice fuelPrice) {
		fuelPriceService.updateFuelPrice(id, fuelPrice);
	}
}