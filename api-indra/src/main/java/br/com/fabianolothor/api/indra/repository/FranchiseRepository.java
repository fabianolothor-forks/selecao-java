package br.com.fabianolothor.api.indra.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.Franchise;

public interface FranchiseRepository extends CrudRepository<Franchise, Integer>{

	Optional<Franchise> findByName(String name);
	
}
