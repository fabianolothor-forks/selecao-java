package br.com.fabianolothor.api.indra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabianolothor.api.indra.model.User;
import br.com.fabianolothor.api.indra.service.UserService;
import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@ApiOperation(value = "Adds a User")
	@PostMapping(value = "")
	public void addUser(@RequestBody User user) {
		userService.addUser(user);
	}
	
	@ApiOperation(value = "Removes a User")
	@DeleteMapping(value = "/{id}")
	public void deleteUser(@PathVariable Integer id) {
		userService.deleteUser(id);
	}
	
	@ApiOperation(response = User.class, value = "Finds a User")
	@GetMapping(value = "/{id}")
	public User getUser(@PathVariable Integer id) {
		return userService.getUser(id);
	}
	
	@ApiOperation(response = List.class, value = "Get all Users")
	@GetMapping(value = "")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@ApiOperation(value = "Updates a User")
	@PutMapping(value = "")
	public void updateUser(@RequestBody User user) {
		userService.updateUser(user);
	}

	@ApiOperation(value = "Updates a User")
	@PutMapping(value = "/{id}")
	public void updateUser(@PathVariable Integer id, @RequestBody User user) {
		userService.updateUser(id, user);
	}

}
