package br.com.fabianolothor.api.indra.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel(description = "Fuel Price Details")
@Builder
@Data
@Entity
@NoArgsConstructor
public class FuelPrice {

	@ApiModelProperty(accessMode = AccessMode.READ_ONLY)
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@ApiModelProperty(required = true)
	private Double buyPrice;

	@ApiModelProperty(required = true)
	@JsonIgnoreProperties("fuelPrices")
	@ManyToOne
	private FuelType fuelType;
	
	@ApiModelProperty(required = true)
	@JsonIgnoreProperties("fuelPrices")
	@ManyToOne
	private GasStation gasStation;

	@ApiModelProperty(required = true)
	private String measurementUnit;
	
	private Double sellPrice;
	
	@ApiModelProperty(required = true)
	private Date verificationDate;

}
