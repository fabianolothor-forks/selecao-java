package br.com.fabianolothor.api.indra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.Data;

@ApiModel(description = "User Details")
@Data
@Entity
public class User {

	@ApiModelProperty(accessMode = AccessMode.READ_ONLY)
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private Integer id;
	
	@ApiModelProperty(required = true)
	@Column(nullable = false, unique = true)
	private String email;
	
	@ApiModelProperty(required = true)
	@Column(nullable = false)
	private String name;
	
	@ApiModelProperty(required = true)
	@Column(nullable = false)
	private String password;
	
	@ApiModelProperty(required = true)
	@Column(nullable = false, unique = true)
	private String username;
	
}
