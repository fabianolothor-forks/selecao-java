package br.com.fabianolothor.api.indra.model;

import lombok.Data;

@Data
public class FuelPriceRecord {

	private String region;
	private String state;
	private String city;
	private String gasStation;
	private Integer gasStationCode;
	private String fuelType;
	private String verificationDate;
	private Double buyPrice;
	private Double sellPrice;
	private String measurementUnit;
	private String franchise;
	
}
