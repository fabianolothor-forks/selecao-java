package br.com.fabianolothor.api.indra.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel(description = "State Details")
@Builder
@Data
@Entity
@NoArgsConstructor
public class State {

	@ApiModelProperty(required = true, value = "National Code")
	@Column(nullable = false, unique = true)
	@Id
	private String id;
	
	@ApiModelProperty(required = true)
	@JsonIgnoreProperties("states")
	@ManyToOne
	private Region region;
	
	@JsonIgnoreProperties("state")
	@OneToMany(mappedBy = "state")
	private List<City> cities;
}
