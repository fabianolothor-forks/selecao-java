package br.com.fabianolothor.api.indra.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabianolothor.api.indra.model.GasStation;
import br.com.fabianolothor.api.indra.repository.GasStationRepository;

@Service
public class GasStationService {

	@Autowired
	private GasStationRepository gasStationRepository;
	
	public List<GasStation> getAllGasStations() {
		List<GasStation> gasStations = new ArrayList<>();
		
		gasStationRepository.findAll().forEach(gasStations::add);
		
		return gasStations;
	}

	public GasStation getGasStation(Integer id) {
		return gasStationRepository.findById(id).orElse(null);
	}
	
}
