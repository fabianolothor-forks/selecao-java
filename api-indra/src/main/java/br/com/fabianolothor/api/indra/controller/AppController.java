package br.com.fabianolothor.api.indra.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
public class AppController {

	@ApiOperation(value = "API Home", response = String.class)
	@GetMapping(value = "/")
	public String home() {
		return "Welcome!";
	}

}
