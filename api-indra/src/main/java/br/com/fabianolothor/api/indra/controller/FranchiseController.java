package br.com.fabianolothor.api.indra.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabianolothor.api.indra.model.Franchise;
import br.com.fabianolothor.api.indra.model.User;
import br.com.fabianolothor.api.indra.service.FranchiseService;
import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/franchises")
public class FranchiseController {

	@Autowired
	FranchiseService franchiseService;
	
	@ApiOperation(response = HashMap.class, value = "Get the average values of all Franchises")
	@GetMapping(value = "/averages")
	public HashMap<Integer, HashMap<String, Double>> getAverages() {
		HashMap<Integer, HashMap<String, Double>> response = new HashMap<Integer, HashMap<String, Double>>();

		List<Franchise> franchises = franchiseService.getAllFranchises();
		
		franchises.forEach(franchise -> {
			response.put(franchise.getId(), franchiseService.getFuelPriceAveragesByFranchise(franchise.getId()));
		});

		return response;
	}
	
	@ApiOperation(response = Franchise.class, value = "Finds a Franchise")
	@GetMapping(value = "/{id}")
	public Franchise getFranchise(@PathVariable Integer id) {
		return franchiseService.getFranchise(id);
	}
	
	@ApiOperation(response = HashMap.class, value = "Get the average values of a Franchise")
	@GetMapping(value = "/{id}/averages")
	public HashMap<String, Double> getFranchiseAverages(@PathVariable Integer id) {
		return franchiseService.getFuelPriceAveragesByFranchise(id);
	}
	
}
