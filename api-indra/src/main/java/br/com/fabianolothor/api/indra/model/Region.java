package br.com.fabianolothor.api.indra.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel(description = "Region Details")
@Builder
@Data
@Entity
@NoArgsConstructor
public class Region {

	@Id
	@Column(nullable = false, unique = true)
	@ApiModelProperty(required = true, value = "National Code")
	private String id;

	@JsonIgnoreProperties("region")
	@OneToMany(mappedBy = "region")
	private List<State> states;
	
}
