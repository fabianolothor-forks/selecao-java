package br.com.fabianolothor.api.indra.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.fabianolothor.api.indra.model.City;
import br.com.fabianolothor.api.indra.model.Franchise;
import br.com.fabianolothor.api.indra.model.FuelPrice;
import br.com.fabianolothor.api.indra.model.FuelPriceRecord;
import br.com.fabianolothor.api.indra.model.FuelType;
import br.com.fabianolothor.api.indra.model.GasStation;
import br.com.fabianolothor.api.indra.model.Region;
import br.com.fabianolothor.api.indra.model.State;
import br.com.fabianolothor.api.indra.repository.CityRepository;
import br.com.fabianolothor.api.indra.repository.FranchiseRepository;
import br.com.fabianolothor.api.indra.repository.FuelPriceRepository;
import br.com.fabianolothor.api.indra.repository.FuelTypeRepository;
import br.com.fabianolothor.api.indra.repository.GasStationRepository;
import br.com.fabianolothor.api.indra.repository.RegionRepository;
import br.com.fabianolothor.api.indra.repository.StateRepository;
import br.com.fabianolothor.api.indra.utils.CSVUtils;

@Service
public class FuelPriceService {

	@Autowired
	private FuelPriceRepository fuelPriceRepository;

	@Autowired
	private FuelTypeRepository fuelTypeRepository;

	@Autowired
	private GasStationRepository gasStationRepository;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private FranchiseRepository franchiseRepository;

	public void addFuelPrice(FuelPrice fuelPrice) {
		if (fuelPrice.getId() == null) {
			fuelPriceRepository.save(fuelPrice);
		}
	}

	public void deleteFuelPrice(Integer id) {
		fuelPriceRepository.deleteById(id);
	}
	
	public List<FuelPrice> getAllByVerificationDate(Date verificationDate) {
		List<FuelPrice> fuelPrices = new ArrayList<>();

		fuelPriceRepository.findAllByVerificationDate(verificationDate).forEach(fuelPrices::add);

		return fuelPrices;
	}

	public List<Date> getAllDistinctVerificationDates() {
		List<Date> dates = new ArrayList<>();

		fuelPriceRepository.findDistinctVerificationDate().forEach(dates::add);

		return dates;
	}

	public List<FuelPrice> getAllFuelPrices() {
		List<FuelPrice> fuelPrices = new ArrayList<>();

		fuelPriceRepository.findAll().forEach(fuelPrices::add);

		return fuelPrices;
	}

	public List<FuelPrice> getAllFuelPricesByRegion(String region) {
		return fuelPriceRepository.getAllByRegion(region);
	}

	public Double getAveragePrice() {
		return fuelPriceRepository.getAveragePrice();
	}

	public Double getAveragePriceByCityName(String cityName) {
		return fuelPriceRepository.getAveragePriceByCityName(cityName);
	}

	public FuelPrice getFuelPrice(Integer id) {
		return fuelPriceRepository.findById(id).orElse(null);
	}

	public void importFuelPriceRecords(MultipartFile file) throws IOException {
		List<FuelPriceRecord> fuelPriceRecords = CSVUtils.read(FuelPriceRecord.class, file.getInputStream());

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		fuelPriceRecords.iterator().forEachRemaining(fuelPriceRecord -> {
			try {
				System.out.println("Importing... " + fuelPriceRecord);

				fuelPriceRepository.save(FuelPrice.builder().buyPrice(fuelPriceRecord.getBuyPrice())
						.fuelType(fuelTypeRepository.save(fuelTypeRepository.findByName(fuelPriceRecord.getFuelType())
								.orElse(FuelType.builder().name(fuelPriceRecord.getFuelType()).build())))
						.gasStation(gasStationRepository.save(gasStationRepository
								.findById(fuelPriceRecord.getGasStationCode())
								.orElse(GasStation.builder().id(fuelPriceRecord.getGasStationCode())
										.city(cityRepository.save(cityRepository.findByName(fuelPriceRecord.getCity())
												.orElse(City.builder().name(fuelPriceRecord.getCity())
														.state(stateRepository.save(stateRepository
																.findById(fuelPriceRecord.getState())
																.orElse(State.builder().id(fuelPriceRecord.getState())
																		.region(regionRepository.save(regionRepository
																				.findById(fuelPriceRecord.getRegion())
																				.orElse(Region.builder()
																						.id(fuelPriceRecord.getRegion())
																						.build())))
																		.build())))
														.build())))
										.franchise(franchiseRepository
												.save(franchiseRepository.findByName(fuelPriceRecord.getFranchise())
														.orElse(Franchise.builder().name(fuelPriceRecord.getFranchise())
																.build())))
										.name(fuelPriceRecord.getGasStation()).build())))
						.measurementUnit(fuelPriceRecord.getMeasurementUnit()).sellPrice(fuelPriceRecord.getSellPrice())
						.verificationDate((Date) formatter.parse(fuelPriceRecord.getVerificationDate())).build());
			} catch (Exception e) {
				System.out.println("Failed\n");

				e.printStackTrace();
			}
		});
	}

	public void updateFuelPrice(FuelPrice fuelPrice) {
		if (fuelPriceRepository.findById(fuelPrice.getId()).isPresent()) {
			fuelPriceRepository.save(fuelPrice);
		}
	}

	public void updateFuelPrice(Integer id, FuelPrice fuelPrice) {
		if (id.equals(fuelPrice.getId()) && fuelPriceRepository.findById(id).isPresent()) {
			fuelPriceRepository.save(fuelPrice);
		}
	}

}
