package br.com.fabianolothor.api.indra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabianolothor.api.indra.model.GasStation;
import br.com.fabianolothor.api.indra.service.GasStationService;
import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/gas-stations")
public class GasStationController {

	@Autowired
	GasStationService gasStationService;
	
	@ApiOperation(response = GasStation.class, value = "Finds a Gas Station")
	@GetMapping(value = "/{id}")
	public GasStation getGasStation(@PathVariable Integer id) {
		return gasStationService.getGasStation(id);
	}

	@ApiOperation(response = List.class, value = "Get all Gas Stations")
	@GetMapping(value = "")
	public List<GasStation> getAllGasStations() {
		return gasStationService.getAllGasStations();
	}
	
}
