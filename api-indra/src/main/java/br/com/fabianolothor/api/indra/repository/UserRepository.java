package br.com.fabianolothor.api.indra.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{

}
