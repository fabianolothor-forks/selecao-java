package br.com.fabianolothor.api.indra.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.FuelType;

public interface FuelTypeRepository extends CrudRepository<FuelType, Integer>{

	public Optional<FuelType> findByName(String name);
	
}
