package br.com.fabianolothor.api.indra.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel(description = "Franchise Details")
@Builder
@Data
@Entity
@NoArgsConstructor
public class Franchise {

	@ApiModelProperty(accessMode = AccessMode.READ_ONLY)
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private Integer id;
	
	@ApiModelProperty(required = true)
	@Column(nullable = false, unique = true)
	private String name;
	
	@JsonIgnoreProperties("franchise")
	@OneToMany(mappedBy = "franchise")
	private List<GasStation> gasStations;
	
}
