package br.com.fabianolothor.api.indra.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabianolothor.api.indra.model.User;
import br.com.fabianolothor.api.indra.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public void addUser(User user) {
		if(user.getId() == null) {
			userRepository.save(user);	
		}
	}
	
	public void deleteUser(Integer id) {
		userRepository.deleteById(id);
	}
	
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		
		userRepository.findAll().forEach(users::add);
		
		return users;
	}

	public User getUser(Integer id) {
		return userRepository.findById(id).orElse(null);
	}
	

	public void updateUser(User user) {
		if(userRepository.findById(user.getId()).isPresent()) {
			userRepository.save(user);
		}
	}
	
	public void updateUser(Integer id, User user) {
		if(id.equals(user.getId()) && userRepository.findById(id).isPresent()) {
			userRepository.save(user);
		}
	}
	
}
