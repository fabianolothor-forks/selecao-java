package br.com.fabianolothor.api.indra.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel(description = "City Details")
@Builder
@Data
@Entity
@NoArgsConstructor
public class City {

	@ApiModelProperty(accessMode = AccessMode.READ_ONLY)
	@Column(nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	@Column(nullable = false)
	@ApiModelProperty(required = true)
	private String name;

	@ApiModelProperty(required = true)
	@JsonIgnoreProperties("cities")
	@ManyToOne
	private State state;
	
	@JsonIgnoreProperties("city")
	@OneToMany(mappedBy = "city")
	private List<GasStation> gasStations;

}
