package br.com.fabianolothor.api.indra.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.fabianolothor.api.indra.model.Region;

public interface RegionRepository extends CrudRepository<Region, String>{

}
