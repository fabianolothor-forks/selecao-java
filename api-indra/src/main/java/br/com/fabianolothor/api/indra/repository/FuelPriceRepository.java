package br.com.fabianolothor.api.indra.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.fabianolothor.api.indra.model.FuelPrice;

public interface FuelPriceRepository extends CrudRepository<FuelPrice, Integer>{
	
	Iterable<FuelPrice> findAllByVerificationDate(Date verificationDate);
	
	@Query("SELECT fp FROM FuelPrice fp WHERE fp.gasStation.city.state.region.id = :region")
	List<FuelPrice> getAllByRegion(@Param("region") String region);
	
	@Query("SELECT AVG(fp.sellPrice) FROM FuelPrice fp")
	Double getAveragePrice();
	
	@Query("SELECT AVG(fp.buyPrice) FROM FuelPrice fp WHERE fp.gasStation.city.name = :cityName")
	Double getAverageCostByCityName(@Param("cityName") String cityName);
	
	@Query("SELECT AVG(fp.buyPrice) FROM FuelPrice fp WHERE fp.gasStation.franchise.id = :id")
	Double getAverageCostByFranchiseID(@Param("id") Integer id);
	
	@Query("SELECT AVG(fp.sellPrice) FROM FuelPrice fp WHERE fp.gasStation.city.name = :cityName")
	Double getAveragePriceByCityName(@Param("cityName") String cityName);
	
	@Query("SELECT AVG(fp.sellPrice) FROM FuelPrice fp WHERE fp.gasStation.franchise.id = :id")
	Double getAveragePriceByFranchiseID(@Param("id") Integer id);
	
	@Query("SELECT DISTINCT fp.verificationDate FROM FuelPrice fp")
	List<Date> findDistinctVerificationDate();
}
