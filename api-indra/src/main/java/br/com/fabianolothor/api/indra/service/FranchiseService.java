package br.com.fabianolothor.api.indra.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabianolothor.api.indra.model.Franchise;
import br.com.fabianolothor.api.indra.repository.FranchiseRepository;
import br.com.fabianolothor.api.indra.repository.FuelPriceRepository;

@Service
public class FranchiseService {

	@Autowired
	private FranchiseRepository franchiseRepository;
	
	@Autowired
	private FuelPriceRepository fuelPriceRepository;

	public List<Franchise> getAllFranchises() {
		List<Franchise> franchises = new ArrayList<>();

		franchiseRepository.findAll().forEach(franchises::add);

		return franchises;
	}

	public Franchise getFranchise(Integer id) {
		return franchiseRepository.findById(id).orElse(null);
	}

	public HashMap<String, Double> getFuelPriceAveragesByFranchise(Integer id) {
		HashMap<String, Double> averages = new HashMap<String, Double>();

		averages.put("averageFuelCost", fuelPriceRepository.getAverageCostByFranchiseID(id));
		averages.put("averageFuelPrice", fuelPriceRepository.getAveragePriceByFranchiseID(id));

		return averages;
	}
}