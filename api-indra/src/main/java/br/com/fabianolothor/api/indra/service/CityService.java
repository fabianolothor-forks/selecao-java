package br.com.fabianolothor.api.indra.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fabianolothor.api.indra.model.City;
import br.com.fabianolothor.api.indra.repository.CityRepository;
import br.com.fabianolothor.api.indra.repository.FuelPriceRepository;

@Service
public class CityService {

	@Autowired
	private FuelPriceRepository fuelPriceRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	public List<City> getAllCities() {
		List<City> cities = new ArrayList<>();
		
		cityRepository.findAll().forEach(cities::add);
		
		return cities;
	}

	public City getCity(Integer id) {
		return cityRepository.findById(id).orElse(null);
	}
	
	public HashMap<String, Double> getFuelPriceAveragesByCity(String cityName) {
		HashMap<String, Double> averages = new HashMap<String, Double>(); 
		
		averages.put("averageFuelCost", fuelPriceRepository.getAverageCostByCityName(cityName));
		averages.put("averageFuelPrice", fuelPriceRepository.getAveragePriceByCityName(cityName));
		
		return averages;
	}
	
}
