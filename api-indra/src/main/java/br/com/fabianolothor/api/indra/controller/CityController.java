package br.com.fabianolothor.api.indra.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fabianolothor.api.indra.model.City;
import br.com.fabianolothor.api.indra.service.CityService;
import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/cities")
public class CityController {

	@Autowired
	CityService cityService;
	
	@ApiOperation(response = HashMap.class, value = "Get the average values of all Cities")
	@GetMapping(value = "/averages")
	public HashMap<String, HashMap<String, Double>> getAverages() {
		HashMap<String, HashMap<String, Double>> response = new HashMap<String, HashMap<String, Double>>();

		List<City> cities = cityService.getAllCities();
		
		cities.forEach(city -> {
			response.put(city.getName(), cityService.getFuelPriceAveragesByCity(city.getName()));
		});

		return response;
	}
	
	@ApiOperation(response = HashMap.class, value = "Get the average values of a City")
	@GetMapping(value = "/{cityName}/averages")
	public HashMap<String, Double> getCityAverages(@PathVariable String cityName) {
		return cityService.getFuelPriceAveragesByCity(cityName);
	}
	
}
