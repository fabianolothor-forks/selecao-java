package br.com.fabianolothor.api.indra.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@ApiModel(description = "Fuel Station Details")
@Builder
@Data
@Entity
@NoArgsConstructor
public class GasStation {

	@ApiModelProperty(required = true, value = "National Code")
	@Column(nullable = false, unique = true)
	@Id
	private Integer id;

	@ApiModelProperty(required = true)
	@JsonIgnoreProperties("gasStations")
	@ManyToOne
	private City city;

	@ApiModelProperty(required = true)
	@JsonIgnoreProperties("gasStations")
	@ManyToOne
	private Franchise franchise;

	@ApiModelProperty(required = true)
	@Column(nullable = false)
	private String name;
	
	@JsonIgnoreProperties("gasStation")
	@OneToMany(mappedBy = "gasStation")
	private List<FuelPrice> fuelPrices;

}
