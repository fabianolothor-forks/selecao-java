### updateUser: Changes the user password

PUT http://localhost:8080/users HTTP/1.1
content-type: application/json

{
  "id": 2,
  "email": "ronaldo@corinthians.com.br",
  "name": "Ronaldo",
  "password": "mynewpassword",
  "username": "ronaldo"
}

### updateUser: Changes the user password with another resource path

PUT http://localhost:8080/users/2 HTTP/1.1
content-type: application/json

{
  "id": 2,
  "email": "ronaldo@corinthians.com.br",
  "name": "Ronaldo",
  "password": "N0WmYp4sSw0rDiSm0r3s4f3tY",
  "username": "ronaldo"
}

### updateUser: This user does not exist, nothing will be done

PUT http://localhost:8080/users/999 HTTP/1.1
content-type: application/json

{
  "id": 999,
  "email": "foo@foo.com",
  "name": "foo",
  "password": "foo",
  "username": "foo"
}

### updateUser: Different IDs between URL and Object, nothing will be done

PUT http://localhost:8080/users/2 HTTP/1.1
content-type: application/json

{
  "id": 999,
  "email": "foo@foo.com",
  "name": "foo",
  "password": "foo",
  "username": "foo"
}
