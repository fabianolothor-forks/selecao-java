### updateUser: Changes the FuelPrice date

PUT http://localhost:8080/fuel-prices/4 HTTP/1.1
content-type: application/json

{
  "id": 5,
  "buyPrice": null,
  "fuelType": {
    "id": 2
  },
  "gasStation": {
    "id": 7890
  },
  "measurementUnit": "R$ / litro",
  "sellPrice": 3.699,
  "verificationDate": "2020-01-03T03:00:00.000+0000"
}
