import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { CityComponent } from './components/city/city.component';
import { FranchiseComponent } from './components/franchise/franchise.component';
import { FuelPriceComponent } from './components/fuel-price/fuel-price.component';
import { GasStationComponent } from './components/gas-station/gas-station.component';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { UserComponent } from './components/user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    CityComponent,
    FranchiseComponent,
    GasStationComponent,
    UserComponent,
    FuelPriceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
