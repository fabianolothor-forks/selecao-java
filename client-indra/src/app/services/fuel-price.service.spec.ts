import { TestBed } from '@angular/core/testing';

import { FuelPriceService } from './fuel-price.service';

describe('FuelPriceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FuelPriceService = TestBed.get(FuelPriceService);
    expect(service).toBeTruthy();
  });
});
