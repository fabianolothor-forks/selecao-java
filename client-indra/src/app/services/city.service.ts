import { EntityAverages } from '../models/EntityAverages';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  citiesURL: string = 'http://localhost:8080/cities';

  constructor(private http: HttpClient) { }

  getCitiesAverages(): Promise<EntityAverages[]> {
    return this.http.get(`${this.citiesURL}/averages`).toPromise().then(response => {
      let entityAverages: EntityAverages[] = [];

      for (let city in response) {
        entityAverages.push({
          name: city,
          averages: {
            cost: response[city].averageFuelCost,
            price: response[city].averageFuelPrice
          }
        })
      }

      return entityAverages;
    });
  }
}
