import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FuelPriceService {
  fuelPricesURL: string = 'http://localhost:8080/fuel-prices';

  constructor(private http: HttpClient) { }

  importCSV(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.fuelPricesURL}/import`, formData);
  };
}
