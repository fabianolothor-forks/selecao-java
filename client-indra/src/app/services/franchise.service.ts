import { EntityAverages } from '../models/EntityAverages';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FranchiseService {
  franchisesURL: string = 'http://localhost:8080/franchises';

  constructor(private http: HttpClient) { }

  async getFranchisesAverages(): Promise<EntityAverages[]> {
    return this.http.get(`${this.franchisesURL}/averages`).toPromise().then(async response => {
      let entityAverages: EntityAverages[] = [];

      for (let franchise in response) {
        entityAverages.push({
          name: await this.http.get(`${this.franchisesURL}/${franchise}`).toPromise().then(response => response['name']),
          averages: {
            cost: response[franchise].averageFuelCost,
            price: response[franchise].averageFuelPrice
          }
        })
      }

      return entityAverages;
    });
  }
}
