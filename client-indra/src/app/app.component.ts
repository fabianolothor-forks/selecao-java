import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  resource: string = '';
  title: string = 'client-indra';

  selectResource(resource: string): void {
    this.resource = resource;
  }
}
