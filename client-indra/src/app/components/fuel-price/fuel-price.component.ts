import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { FuelPriceService } from '../../services/fuel-price.service';

@Component({
  selector: 'app-fuel-price',
  templateUrl: './fuel-price.component.html',
  styleUrls: ['./fuel-price.component.css']
})
export class FuelPriceComponent implements OnInit {
  action: number = 0;
  uploadForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private fuelPriceService: FuelPriceService) { }

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      file: ['']
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('file').value);

    this.fuelPriceService.importCSV(formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );;
  }

  selectAction(action: number): void {
    this.action = action;
  }

}
