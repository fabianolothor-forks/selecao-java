import { Component, OnInit } from '@angular/core';

import { CityService } from '../../services/city.service';
import { EntityAverages } from '../../models/EntityAverages';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  action: number = 0;
  citiesAverages: EntityAverages[];

  constructor(private cityService: CityService) { }

  async ngOnInit() {
    this.citiesAverages = await this.cityService.getCitiesAverages();
  }

  selectAction(action: number): void {
    this.action = action;
  }

}
