import { Component, OnInit } from '@angular/core';

import { EntityAverages } from '../../models/EntityAverages';
import { FranchiseService } from '../../services/franchise.service';

@Component({
  selector: 'app-franchise',
  templateUrl: './franchise.component.html',
  styleUrls: ['./franchise.component.css']
})
export class FranchiseComponent implements OnInit {
  action: number = 0;
  franchisesAverages: EntityAverages[];

  constructor(private franchiseService: FranchiseService) { }

  async ngOnInit() {
    this.franchisesAverages = await this.franchiseService.getFranchisesAverages();
  }

  selectAction(action: number): void {
    this.action = action;
  }

}
