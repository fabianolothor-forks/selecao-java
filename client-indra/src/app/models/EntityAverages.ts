import { Averages } from './Averages'

export class EntityAverages {
  name: string;
  averages: Averages;
}
